# Text Adventure Game
# Version 1.1

Text adventure game written in under 8 hours. There are definitely things that could be changed to improve the functionality. For example, methods could have been called for the GAME OVER messages instead of repeating each time it was needed. It could also be edited to loop back around when the player selects an incorrect letter instead of punishing the player by exiting from the game. Case statements would also make the code cleaner than using if statements.

These are all changes that I hope to make at a later date.